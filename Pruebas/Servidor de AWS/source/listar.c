#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/listar.h"

void listar()
{
	printf("\n\n*** LISTAR ***\n");

	// PASOS DE PARSEO:
	// 1) Abrir el archivo con los datos y asociarlo a un puntero del tipo FILE.
	FILE *archivo=fopen("./data/datos_meteorologicos.CSV","r");

	// 2) Generar un arreglo de caracteres (string) para almacenar cada línea extraída del archivo de datos. Se lo limpia con "memset".
	char data[1000];
    memset(data,0,sizeof(data));

    // 3) Generar un puntero de tipo "char" que apuntará a los datos parseados.
    char *estacion;
    // 4) Generar un string donde se guardará el valor de la estacion anteriormente relevada (esto sirve para comparar y listar solo las estaciones diferentes)
    char estacion_guardada[20];
    memset(estacion_guardada,0,sizeof(estacion_guardada));
    // 5) Puntero a char donde se guardan los demás elementos de cada línea
    char *parseo;
    // 6) Array de punteros a char que guarda los titulos de los elementos de cada linea (es decir, el nombre de lo sensado) (20 elementos de hasta 10 caracteres c/u)
    char titulos[20][100];
	memset(titulos,0,sizeof(titulos));


    // 4) Usar "fgets" que permite extraer una por una las líneas del archivo. Ya que las dos primeras líneas contienen datos que no son de interés inmediato, 
    // se parsean en esta parte y se muestran en pantalla.
    for(int n=0;n<2;n++)
    {
    	fgets(data,1000,(FILE*)archivo);
	    //printf("%s\n", data);
    }

    /*
    Se extrae la tercera linea con los titulos de cada elemento
    */   
	fgets(data,1000,(FILE*)archivo);
	for(int m=0;m<20;m++)
    {
    	// 5.1.1) Si se trata del primer elemento de la línea, se establece el puntero a "data" y se parsea el primer elemento
    	// (que se encuentra antes de la primera coma). Se lo copia al array de char "titulos" y se lo muestra en pantalla
    	if(m==0)
    	{
    		estacion=strtok(data,",");
    		strcpy(titulos[m],estacion);
    		//printf("%s ",titulos[m]);	
    		
    	}
    	// 5.1.2) Para los siguientes elementos de cada línea, el puntero a "data" ya está fijado (por eso se pasa NULL a "strtok") y se parsean.
    	else
    	{
    		parseo=strtok(NULL,",");
    		strcpy(titulos[m],parseo);
    		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
    		//printf("%s ",titulos[m]);	
    	}
    }    
	printf("\n");    


	// 5.1) Se itera sobre los 20 elementos que componen cada línea
	fgets(data,1000,(FILE*)archivo);
	for(int i=0;i<20;i++)
    {
    	// 5.1.1) Si se trata del primer elemento de la línea, se establece el puntero a "data" y se parsea el primer elemento
    	// (que se encuentra antes de la primera coma).
    	if(i==0)
    	{
    		estacion=strtok(data,",");
    		printf("%s ",estacion);	
    		strcpy(estacion_guardada,estacion);
    	}
    	// 5.1.2) Para los siguientes elementos de cada línea, el puntero a "data" ya está fijado (por eso se pasa NULL a "strtok") y se parsean.
    	else
    	{
    		parseo=strtok(NULL,",");
    		if(i==1)
    		{
    			printf("%s: Sensores habilitados:\n",parseo);
    		}
    		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
    		if(strcmp("--",parseo) && i>3)
    		{
    			//printf("%s = %s ",titulos[i],parseo);
    			printf("%s  ",titulos[i]);
    		}
    	}
    }
    // 5.2) Se separa cada línea de elementos con un salto de línea que se imprime en pantalla.
    printf("\n\n");    

    // 5) Se obtienen los datos de cada fila y se almacenan en "data". También se corrobora que no se haya llegado al EOF.

    while(fgets(data,1000,(FILE*)archivo) != NULL){
		// Se guarda el primer elemento de la línea en "siguiente_estacion"
	    estacion=strtok(data,",");
	    // printf("Estacion = %s\n",estacion);
	    // printf("Siguiente estacion = %s\n",siguiente_estacion);
	    // Se lo compara con el elemento guardado de la línea anterior. Si son distintos, se muestra la nueva linea en pantalla
	    if(strcmp(estacion,estacion_guardada))
	    {
	    	printf("%s ",estacion);
	    	for(int j=0;j<19;j++)
	    	{
	    		parseo=strtok(NULL,",");
		    	// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
		    	if(j==0)
	    		{
	    			printf("%s: Sensores habilitados:\n",parseo);
	    		}
	    		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
	    		if(strcmp("--",parseo) && j>2)
	    		{
	    			//printf("%s = %s ",titulos[j+1],parseo);
	    			printf("%s  ",titulos[j+1]);
	    		}
	    	}
	    	printf("\n\n");
	    	memset(estacion_guardada,0,sizeof(estacion_guardada));
	    	strcpy(estacion_guardada,estacion);
	    }
    }
    int cerrado = fclose(archivo);
}