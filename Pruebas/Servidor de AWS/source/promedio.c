#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/promedio.h"

/*
PROMEDIO debe combinar el funcionamiento de DESCARGAR (que permite obtener todos los datos de una sola estacion), LISTAR (que permite
obtener los nombres de cada variable), y MENSUAL PRECIPITACION (que permite operar sobre todos los datos de una variable a lo largo de todo el mes)
*/

void promedio(char *nro_estacion)
{
	// PASOS DE PARSEO:
	// 1) Abrir el archivo con los datos y asociarlo a un puntero del tipo FILE.
	FILE *archivo=fopen("./data/datos_meteorologicos.CSV","r");

	// 2) Generar un arreglo de caracteres (string) para almacenar cada línea extraída del archivo de datos. Se lo limpia con "memset".
	char data[1000];
    memset(data,0,sizeof(data));

    // 3) Generar un puntero de tipo "char" que apuntará a los datos parseados.
    char *parseo;

    int estacion_ok;

    // 4) Usar "fgets" que permite extraer una por una las líneas del archivo. Ya que las tres primeras líneas contienen datos que no son de interés inmediato, 
    // se parsean en esta parte y se muestran en pantalla.
    for(int n=0;n<3;n++)
    {
    	fgets(data,1000,(FILE*)archivo);
	    //printf("%s\n", data);
    }   

    printf("\n\n*** PROMEDIO POR VARIABLE DE LA ESTACION %s ***\n",nro_estacion);

    // 5) Se obtienen los datos de cada fila y se almacenan en "data". También se corrobora que no se haya llegado al EOF.
    while(fgets(data,1000,(FILE*)archivo) != NULL)
    {
    	// 5.1) Se itera sobre los 20 elementos que componen cada línea
  	    for(int i=0;i<20;i++)
	    {
	    	// 5.1.1) Si se trata del primer elemento de la línea, se establece el puntero a "data" y se parsea el primer elemento
	    	// (que se encuentra antes de la primera coma).
	    	if(i==0)
	    	{
	    		parseo=strtok(data,",");
	    		estacion_ok = strcmp(parseo,nro_estacion);
	    		if(!estacion_ok)
	    		{
	    			printf("%s ",parseo);
	    		}
	    	}
	    	// 5.1.2) Para los siguientes elementos de cada línea, el puntero a "data" ya está fijado (por eso se pasa NULL a "strtok") y se parsean.
	    	else if(!estacion_ok)
	    	{
	    		parseo=strtok(NULL,",");
	    		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
	    		printf("%s ",parseo);	
	    	}
	    }  
	    if(!estacion_ok)
	    {
	    	printf("\n");
	    } 	
    }

    int cerrado = fclose(archivo);
}