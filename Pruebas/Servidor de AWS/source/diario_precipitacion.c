#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/diario_precipitacion.h"

/* 
¿COMO CASTEAR A FLOAT O DOUBLE LOS CHAR DE PRECIPITACION?
http://stackoverflow.com/questions/13424265/convert-a-char-to-double

Hacer el parseo por token de la variable fecha y hora para acumular por dia
Para hacerlo menos complicado podria simplemente copiar los dos primeros valores del campo fechahora que es donde se encuentra el nro de dia
y hacer una comparacion para saber si ya cambio
*/

void diario_precipitacion(char *nro_estacion)
{
	// PASOS DE PARSEO:
	// 1) Abrir el archivo con los datos y asociarlo a un puntero del tipo FILE.
	FILE *archivo=fopen("./data/datos_meteorologicos.CSV","r");

	// 2) Generar un arreglo de caracteres (string) para almacenar cada línea extraída del archivo de datos. Se lo limpia con "memset".
	char data[1000];
    memset(data,0,sizeof(data));
    char data_copia[1000];
    memset(data_copia,0,sizeof(data_copia));

    // 3) Generar un puntero de tipo "char" que apuntará a los datos parseados.
    char *parseo;
    // Flag de estacion seleccionada
    int estacion_ok;

    // Valor diario acumulado
    double acumulado_double=0;
    char acumulado_char[10];
    memset(acumulado_char,0,sizeof(acumulado_char));

    // // String con todos los datos de la estacion y su acumulado mensual
    char full_acumulado[1000];
    memset(full_acumulado,0,sizeof(full_acumulado));
    int concat_full=1; // Flag para concatenacion del acumulado

    // Conteo de muestras tomadas. Cada 144 muestras se completa un dia -> ESTO NO SIRVE; ELIMINAR
    int conteo_muestras=0;

    //aca se almacena el nro de dia
    char nro_dia_anterior[2]="01";    
	char nro_dia_actual[2];    
    memset(nro_dia_actual,0,sizeof(nro_dia_actual));

    int ultimo_valor=0; // para devolver el acumulado diario del ultimo dia antes


    // 4) Usar "fgets" que permite extraer una por una las líneas del archivo. Ya que las tres primeras líneas contienen datos que no son de interés inmediato, 
    // se parsean en esta parte y se muestran en pantalla.
    for(int n=0;n<3;n++)
    {
    	fgets(data,1000,(FILE*)archivo);
	    //printf("%s\n", data);
    }   

    printf("\n\n*** ACUMULADO DIARIO DE PRECIPITACION DE LA ESTACION %s ***\n",nro_estacion);

    //5) Se obtienen los datos de cada fila y se almacenan en "data". También se corrobora que no se haya llegado al EOF.
    while(fgets(data,1000,(FILE*)archivo) != NULL)
    {
    	strcpy(data_copia,data);
    	// Se hace una primera recorrida por la linea recien extraida del archivo para verificar si corresponde al mismo dia que la anterior
    	for(int i=0;i<8;i++)
    	{
    		if(i==0)
    		{
    			parseo=strtok(data_copia,","); // se genera el puntero al primer elemento de la linea extraida del archivo
	    		estacion_ok = strcmp(parseo,nro_estacion);
	    		//estacion_ok=0;

    		}
    		else if(!estacion_ok)
    		{
    			parseo=strtok(NULL,","); // se apunta a los siguiente elementos de la linea extraida del archivo
    			if(i==3) // este es el campo fecha hora 
    			{
    				strncpy(nro_dia_actual,parseo,2); // se copian los dos caracteres donde está el nro de dia
    				if(strcmp(nro_dia_actual,nro_dia_anterior)) // si la comparacion devuelve que son distintos
    				{
					    printf("%s\n",full_acumulado); // se imprime en pantalla el concatenado resultante
					    acumulado_double=0; // acumulado vuelve a cero para empezar a sumar de nuevo lo del dia siguiente
					    strcpy(nro_dia_anterior,nro_dia_actual);
					    ultimo_valor=1;
    				}
    			}
    		}
    		//else if(!strcmp(nro_dia_actual,"28"))
    		else if(estacion_ok && ultimo_valor)
    		{
    			printf("%s\n",full_acumulado); // se imprime en pantalla el concatenado resultante
			    //acumulado_double=0; // acumulado vuelve a cero para empezar a sumar de nuevo lo del dia siguiente
			    //strcpy(nro_dia_actual,"01");
			    ultimo_valor=0;
    		}
    	}
    	// 5.1) Se itera sobre los 20 elementos que componen cada línea
  	    for(int i=0;i<8;i++)
	    {
	    	// 5.1.1) Si se trata del primer elemento de la línea, se establece el puntero a "data" y se parsea el primer elemento
	    	// (que se encuentra antes de la primera coma).
	    	if(i==0)
	    	{
	    		parseo=strtok(data,",");
	    		//estacion_ok = strcmp(parseo,nro_estacion);
	    		if(!estacion_ok)
	    		{
	    			//printf("%s ",parseo);
	    			strcpy(full_acumulado,parseo);
	    			strcat(full_acumulado," ");
	    		}
	    	}
	    	// 5.1.2) Para los siguientes elementos de cada línea, el puntero a "data" ya está fijado (por eso se pasa NULL a "strtok") y se parsean.
	    	else if(!estacion_ok)
	    	{
	    		parseo=strtok(NULL,",");
	    		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
	    		if(i<4)
	    		{
	    			//printf("%s ",parseo);		
	    			strcat(full_acumulado,parseo);
	    			strcat(full_acumulado," ");	    			
	    			//printf("%s\n",full_acumulado);
	    		}
	    		if(i==7)
	    		{
	    			acumulado_double+=atof(parseo);
	    			sprintf(acumulado_char, "%g", acumulado_double);
		    		strcat(full_acumulado,acumulado_char);
		    		//printf("%s\n",full_acumulado); // se imprime en pantalla el concatenado resultante
	    			//printf("%f\n ",acumulado_double);
	    		}
	    	}
	    }  
    }
    printf("%d\n", estacion_ok);
    printf("%d\n", ultimo_valor);
    if(!estacion_ok && ultimo_valor)
	{
		printf("%s\n",full_acumulado); // se imprime en pantalla el concatenado resultante
	    //acumulado_double=0; // acumulado vuelve a cero para empezar a sumar de nuevo lo del dia siguiente
	    //strcpy(nro_dia_actual,"01");
	    ultimo_valor=0;
	}

    int cerrado = fclose(archivo);
}