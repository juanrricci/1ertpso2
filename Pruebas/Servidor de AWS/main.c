#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include "./headers/listar.h"
#include "./headers/descargar.h"
#include "./headers/mensual_precipitacion.h"
#include "./headers/diario_precipitacion.h"


int main(int argc, char *argv[]) 
{
	// PASOS DE PARSEO:
	// 1) Abrir el archivo con los datos y asociarlo a un puntero del tipo FILE.
	FILE *archivo=fopen("./data/datos_meteorologicos.CSV","r");

	// 2) Generar un arreglo de caracteres (string) para almacenar cada línea extraída del archivo de datos. Se lo limpia con "memset".
	char data[1000];
    memset(data,0,sizeof(data));

    // 3) Generar un puntero de tipo "char" que apuntará a los datos parseados.
    char *parseo;

    // 4) Usar "fgets" que permite extraer una por una las líneas del archivo. Ya que las tres primeras líneas contienen datos que no son de interés inmediato, 
    // se parsean en esta parte y se muestran en pantalla.
    for(int n=0;n<3;n++)
    {
    	fgets(data,1000,(FILE*)archivo);
	    printf("%s\n", data);
    }   

    // 5) Se obtienen los datos de cada fila y se almacenan en "data". También se corrobora que no se haya llegado al EOF.
    while(fgets(data,1000,(FILE*)archivo) != NULL){
    	// 5.1) Se itera sobre los 20 elementos que componen cada línea
  	    for(int i=0;i<20;i++)
	    {
	    	// 5.1.1) Si se trata del primer elemento de la línea, se establece el puntero a "data" y se parsea el primer elemento
	    	// (que se encuentra antes de la primera coma).
	    	if(i==0)
	    	{
	    		parseo=strtok(data,",");
	    	}
	    	// 5.1.2) Para los siguientes elementos de cada línea, el puntero a "data" ya está fijado (por eso se pasa NULL a "strtok") y se parsean.
	    	else
	    	{
	    		parseo=strtok(NULL,",");
	    	}
	 		
	 		// 5.1.3) Se muestran uno al lado del otro (separados por un espacio) los elementos de cada línea.
	    	printf("%s ",parseo);	
	    }
	    // 5.2) Se separa cada línea de elementos con un salto de línea que se imprime en pantalla.
	    printf("\n");    	
    }

    int cerrado = fclose(archivo);

    listar();

    //descargar("30069");

    mensual_precipitacion("30099");

    diario_precipitacion("30099");
}

	// char *parseo=strtok(data," \n\t");
	// printf("%s\n",parseo);

	// for(int i=0;i<20;i++)
	// {
	// 	parseo=strtok(NULL,",");
	// 	printf("%s ",parseo);
	// }

	// /*
	// Elimininacion del archivo DATA que pueda quedar
	// de una ejecucion anterior
	// */ 
	// system("rm ./files/data.txt");

	// /*
	// Creacion del semaforo necesario para el acceso a 
	// DATA
	// */
	// sem_t *sem = sem_open( "/MyTestSemaphore", O_CREAT | // create if it does not already exist 
	// O_CLOEXEC , // close on execute 
	// S_IRUSR | // user permission: read 
	// S_IWUSR , // user permission: write 
	// 1 ); // initial value of the semaphore

	// /*
	// Primer fork para generar el "capturador de datos"
	// desde los sensores
	// */
	// int pidCD = fork();
	// if(pidCD >= 0) { // fork realizado correctamente
	// 		if(pidCD == 0) {
	// 			// Hijo que ejecuta la funcion de manejo de archivos
	// 			//printf("PID de captureData: %d\n", getpid());
	// 			captureData(argc, argv, sem);
	// 		}
	// }
	
	// /*
	// Segundo fork para levantar el servidor TCP
	// */
	// int pidTCP = fork();
	// if(pidTCP >= 0) { // fork realizado correctamente
	// 		if(pidTCP == 0) {
	// 			//printf("Fork de serverTCP: %d\n", getpid());
	// 			serverTCP(argc, argv, sem);
	// 		}
	// }

	// /* 
	// El proceso padre ejecuta el servidor UDP
	// */
	// //printf("Proceso serverUDP: %d\n", getpid());
	// serverUDP(argc, argv, sem);

 //    return 0;


