#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <semaphore.h>

#define TAM 256

#include "./headers/connectComm.h"
#include "./headers/combinedClient.h"

int main(int argc, char *argv[]) {

    system("rm ./files/rcvdData.txt");

    int r = 1; // flag
    char command[30]; // seteo el string donde guardare los comandos
    char hosting[30];
    char porto[30];

    memset(&hosting[0], 0, sizeof(hosting));
    memset(&porto[0], 0, sizeof(porto));

    printf("*** BIENVENIDO AL COP DEL SMN ***\n");

    while(r != 0) {

        memset(&command[0], 0, sizeof(command)); // limpio el buffer command
        printf("Por favor, ingrese 'connect IP PORT' de la AWS a la que desea conectarse:\n");
        printf("==> ");
        fgets(command, sizeof(command), stdin); // se recibe el comando y se lo guarda en "command"

        //printf("%s\n", command);

        r = connectComm(command, sizeof(command), hosting, porto); // se ejecuta la funcion "connectComm" para validar los comandos ingresados
    }

    printf("Por favor, ingrese alguno de los siguientes comandos:\n\n");
    printf("get_telemetry: le proveerá el último registro capturado por la AWS.\n\n");
    printf("get_data: le proveerá todos los registros capturados por la AWS hasta este momento.\n\n");
    printf("erase_data: eliminará todos los registros capturados y almacenados en la AWS.\n\n");
    printf("disconnect: cerrará la conexión entre este cliente y la AWS.\n\n");

    combinedClient(hosting, porto);

    return 0;
}
