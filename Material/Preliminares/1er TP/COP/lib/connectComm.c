#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/connectComm.h"

int connectComm(char *command, int sizeCommand, char *hosting, char *porto) {

	char validate[30];
    char connect[] = "connect";
    int comandoValido;
    int i, j, k, l; // contadores

    memset(&validate[0], 0, sizeof(validate));

    // se copian los primeros 7 caracteres de "command" a "validate"
    strncpy(validate, command, 7); 

    //printf("esto es validate : %s\n", validate);

    // se compara lo guardado en "validate" con el string "connect" para chequear si el primer comando es el correcto
    if(strcmp(validate, connect) == 0) {
        //printf("se ingreso el comando connect correctamente\n\n");
    } else {
    	/* si el primer comando, que debería ser "connect" no lo es, entonces se retorna al main para 
    	   solicitar de nuevo el comando 
    	*/
        printf("Comandos incorrectos.\n\n");
        return 1;
    }
    
    j = 0;
    k = 0;
    l = 0;
    /*
    con el siguiente "for" se comprueba si se ingresaron 3 argumentos (mediante el conteo de espacios en blanco)
    */
    for(i = 0; i < sizeCommand; i++) {

        if(command[i] == ' ') {
            j++;
            //printf("%c ok\n", command[i]);
        } else if(j == 1 && command[i] != ' ') {
        	hosting[k] = command[i];
        	k++;
        	// en este "if" se parsea el host
            	
        } else if(j == 2 && command[i] != ' ') {
        	porto[l] = command[i];
        	l++;
            // en este "if" se parsea el puerto
        }
    }

    /*
    si la cantidad de comandos es distinta de 3, entonces se retorna 1 al main para volver a pedir los comandos
    adecuados
    */

    if(j < 2) {        
        //printf("Cantidad de comandos menor a la necesaria\n\n");
        printf("Comandos incorrectos.\n\n");
        return 1;
    } else if (j == 2) {
        printf("Conexión aceptada.\n\n");
        return 0;
    } else {
        //printf("Cantidad de comandos mayor a la necesaria\n\n");
        printf("Comandos incorrectos.\n\n");      
        return 1;
    }

    return 0;

}
