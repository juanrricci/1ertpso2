#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define TAM 256

#include "../headers/combinedClient.h"

int combinedClient( const char *hosting, const char *porto ) {

	// *** encabezado TCP

	int sockfdTCP, puertoTCP, n;
	struct sockaddr_in serv_addrTCP;
	struct hostent *serverTCP;
	int terminar = 0;
	int p;

	char buffer[TAM];
	char buffcpy[TAM];

	FILE *receivedData;

	puertoTCP = atoi( porto );
	sockfdTCP = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sockfdTCP < 0 ) {
		perror( "ERROR apertura de socket" );
		exit( 1 );
	}

	serverTCP = gethostbyname( hosting );
	if (serverTCP == NULL) {
		fprintf( stderr,"Error, no existe el host\n" );
		exit( 0 );
	}
	memset( (char *) &serv_addrTCP, '0', sizeof(serv_addrTCP) );
	serv_addrTCP.sin_family = AF_INET;
	bcopy( (char *)serverTCP->h_addr, (char *)&serv_addrTCP.sin_addr.s_addr, serverTCP->h_length );
	serv_addrTCP.sin_port = htons( puertoTCP );
	if ( connect( sockfdTCP, (struct sockaddr *)&serv_addrTCP, sizeof(serv_addrTCP ) ) < 0 ) {
		perror( "conexion" );
		exit( 1 );
	}

	// *** encabezado UDP

	int sockfdUDP, puertoUDP, o, tamano_direccion;
	struct sockaddr_in dest_addr;
	struct hostent *server;

	server = gethostbyname( hosting );
	if ( server == NULL ) {
		fprintf( stderr, "ERROR, no existe el host\n");
		exit(0);
	}

	puertoUDP = atoi( porto );
	sockfdUDP = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfdUDP < 0) {
		perror( "apertura de socket" );
		exit( 1 );
	}

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons( atoi( porto ) );
	dest_addr.sin_addr = *( (struct in_addr *)server->h_addr );
	memset( &(dest_addr.sin_zero), '\0', 8 );

	tamano_direccion = sizeof( dest_addr );	

	while(1) {
		printf( "AWS@%s:%d> ",hosting, puertoTCP );
		memset( buffer, '\0', TAM );
		fgets( buffer, TAM-1, stdin );

		strcpy(buffcpy, buffer);
		buffcpy[strlen(buffcpy)-1] = '\0';

		if( strcmp( "disconnect", buffcpy ) && strcmp( "get_data", buffcpy ) && strcmp( "get_telemetry", buffcpy ) && strcmp( "erase_data", buffcpy )) {
			printf("El comando ingresado no es válido. Por favor, ingrese get_telemetry, get_data, erase_data o disconnect.\n\n");
			continue;
		}

		n = write( sockfdTCP, buffer, strlen(buffer)); // <=========== **** WRITE **** (a ser cambiado por "send" para envio bloqueante)
		if ( n < 0 ) {
			perror( "escritura de socket" );
			exit( 1 );
		}

		// *** INICIO ENVio Y RECEPCION UDP
		o = sendto( sockfdUDP, (void *)buffer, TAM, 0, (struct sockaddr *)&dest_addr, tamano_direccion );
		if ( o < 0 ) {
			perror( "Escritura en socket" );
			exit( 1 );
		}

		// Verificando si se escribió: disconnect
		if( !strcmp( "disconnect", buffcpy ) ) {
			terminar = 1;

		} else if(!strcmp( "get_telemetry", buffcpy ) ) {
			receivedData = fopen("./files/rcvdData.txt","a");
			
			memset( buffer, 0, sizeof( buffer ) );
			
			o = recvfrom( sockfdUDP, (void *)buffer, TAM, 0, (struct sockaddr *)&dest_addr, &tamano_direccion );
			if ( o < 0 ) {
				perror( "Lectura de socket" );
				exit( 1 );
			}

            fputs("\n\n*** GET_TELEMETRY ***\n\n", receivedData); // ingresa en "data" lo que tiene almacenado en el buffer
            fputs(buffer, receivedData);
            fclose(receivedData);

			//printf( "Respuesta por UDP ingresada a file: %s\n", buffer );			


		} else if(!strcmp( "get_data", buffcpy ) ) {
			p = 1;
			receivedData = fopen("./files/rcvdData.txt","a");
			fputs("\n\n*** GET_DATA ***\n\n", receivedData); // ingresa en "data" lo que tiene almacenado en el buffer
			
			while(p) {
				memset( buffer, 0, sizeof( buffer ) );
				
				o = recvfrom( sockfdUDP, (void *)buffer, TAM, 0, (struct sockaddr *)&dest_addr, &tamano_direccion );
				if ( o < 0 ) {
					perror( "Lectura de socket" );
					exit( 1 );
				}		
           		
           		fputs(buffer, receivedData);		
				
				if(!strcmp(buffer, "fin")) {
					p = 0;
				}			
			}

			fclose(receivedData);

		} else {
			memset( buffer, 0, sizeof( buffer ) );
			o = recvfrom( sockfdUDP, (void *)buffer, TAM, 0, (struct sockaddr *)&dest_addr, &tamano_direccion );
			if ( o < 0 ) {
				perror( "Lectura de socket" );
				exit( 1 );
			}
			//printf( "Respuesta por UDP: %s\n", buffer );			
		}

		// *** FIN ENVio Y RECEPCION UDP			

		memset( buffer, '\0', TAM );
		n = read( sockfdTCP, buffer, TAM );// <=========== **** READ ****
		if ( n < 0 ) {
			perror( "lectura de socket" );
			exit( 1 );
		}
		printf( "Respuesta: %s\n", buffer );

		if( terminar ) {
			printf( "Finalizando ejecución\n" );
			exit(0);
		}
	}	

	return 0;
}