#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include "../headers/captureData.h"
#include "../headers/serverTCP.h"

int captureData(int cantArg, char *pointArg[], sem_t *sem) {

    FILE *exterior; // puntero a archivo q simula a los sensores
    FILE *data; // puntero a archivo donde se guardan las mediciones
    char entrada[255]; // buffer q permite pasar los datos desde "exterior" a "data"
    int i = 0; // contador

    printf("Proceso captureData: %d\n", getpid());
    exterior = fopen("./files/delta.csv","r"); // abre el archivo q simula a los sensores

    /*
    El siguiente bucle itera para ir captando cada linea de "exterior" e ir guardandola en "data".
    Tiene una condicion (en el "if" interior) para no guardar la primera linea de "exterior", 
    que son los encabezados de los datos que tiene guardados.
    */

    while(!feof(exterior)) { 

            memset(&entrada[0], 0, sizeof(entrada)); // se limpia el buffer con cada iteracion
            fgets(entrada, 255, (FILE*)exterior); // guarda en el buffer "entrada" cada linea de "exterior"

            if(i != 0) {

                sem_wait(sem); // toma el semaforo para escribir en "data"

                data = fopen("./files/data.txt","a"); // abre "data" y lo prepara para agregarle datos
                fputs(entrada, data); // ingresa en "data" lo que tiene almacenado en el buffer
                fclose(data); // cierra "data"
                
                sem_post(sem); // devuelve el semaforo para que "data" pueda ser accedido por otro proceso
                //printf("%s", entrada);
                sleep(2); // el proceso duerme durante 1 segundo antes de sensar el siguiente dato

            }

            i++;
    }

    fclose(exterior); // cierra el archivo que simula a los sensores
    exit(0); // system call para finalizar el proceso de lectura/escritura de archivos

    return 0;
}