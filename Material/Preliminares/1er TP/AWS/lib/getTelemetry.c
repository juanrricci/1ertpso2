#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include "../headers/getTelemetry.h"

int getTelemetry(char *respuesta, sem_t *sem) {

	FILE *data;
	char entrada[255];

	sem_wait(sem);

	data = fopen("./files/data.txt", "r");

	while(!feof(data)) {
		fgets(entrada, 255, (FILE*)data);
	}
	sleep(1);
	printf("%s\n", entrada);

	strcpy(respuesta, entrada);

	fclose(data);

	sem_post(sem);

	return 0;
}