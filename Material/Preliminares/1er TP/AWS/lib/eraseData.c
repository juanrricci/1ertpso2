#include <stdlib.h>
#include <unistd.h>

#include <semaphore.h>
#include "../headers/eraseData.h"

void eraseData(sem_t *sem) {

	sem_wait(sem);
	
	system("rm ./files/data.txt");

	sleep(1);

	sem_post(sem);
}