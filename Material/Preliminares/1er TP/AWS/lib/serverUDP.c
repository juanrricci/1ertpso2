#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define TAM 256

#include "../headers/serverUDP.h"
#include "../headers/getTelemetry.h"
#include "../headers/getData.h"

int serverUDP(int cantArg, char *pointArg[], sem_t *sem) {  
	int sockfd, puerto, tamano_direccion;
	char buffer[ TAM ];
	char respuesta[40];
	struct sockaddr_in serv_addr;
	int n,p;

	if ( cantArg < 2 ) {
        	fprintf( stderr, "Uso: %s <puerto>\n", pointArg[0] );
		exit( 1 );
	}

	sockfd = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd < 0) { 
		perror("ERROR en apertura de socket");
		exit( 1 );
	}

	memset( &serv_addr, 0, sizeof(serv_addr) );
	puerto = atoi( pointArg[1] );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );
	memset( &(serv_addr.sin_zero), '\0', 8 );

	if( bind( sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0 ) {
		perror( "ERROR en binding" );
		exit( 1 );
	}

	printf("Proceso serverUDP: %d - socket disponible: %d\n", getpid(), ntohs(serv_addr.sin_port));
	tamano_direccion = sizeof( struct sockaddr );
	while ( 1 ) {
		memset( buffer, 0, TAM );
		n = recvfrom( sockfd, buffer, TAM-1, 0, (struct sockaddr *)&serv_addr, &tamano_direccion );
		if ( n < 0 ) {
			perror( "lectura de socket" );
			exit( 1 );
		}
		printf( "Recibí por UDP: %s", buffer );

        // Verificación de si hay que terminar
        buffer[strlen(buffer)-1] = '\0';

	    if(!strcmp("get_telemetry", buffer)) {
	        getTelemetry(respuesta, sem);		
			n = sendto( sockfd, respuesta, sizeof(respuesta), 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
			if ( n < 0 ) {
				perror( "escritura en socket" );
				exit( 1 );
			}
	    } else if(!strcmp("get_data", buffer)) { 
	    	getData(sockfd, tamano_direccion, serv_addr, sem);
	    } else {
			n = sendto( sockfd, (void *)"Obtuve su mensaje", 18, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
			if ( n < 0 ) {
				perror( "escritura en socket" );
				exit( 1 );
			}
		}	    	
	}

	return 0; 
} 
