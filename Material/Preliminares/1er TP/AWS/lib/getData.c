#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include "../headers/getData.h"

int getData(int sockfd, int tamano_direccion, struct sockaddr_in serv_addr, sem_t *sem) {

	FILE *data;
	char entrada[255];
	int n;

	sem_wait(sem);

	data = fopen("./files/data.txt", "r");

	while(!feof(data)) {
		fgets(entrada, 255, (FILE*)data);
		printf("%s\n", entrada);
		n = sendto( sockfd, entrada, sizeof(entrada), 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
		sleep(1);
	}

	fclose(data);

	sem_post(sem);

	printf("fin\n");
	n = sendto( sockfd, "fin", 5, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );

	return 0;
}