#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define TAM 256

#include "../headers/serverTCP.h"
#include "../headers/eraseData.h"

int serverTCP(int cantArg, char *pointArg[], sem_t *sem) {   
	int sockfd, newsockfd, puerto, clilen, pid;
	char buffer[TAM];
	struct sockaddr_in serv_addr, cli_addr;
	int n;

	if ( cantArg < 2 ) {
        	fprintf( stderr, "Uso: %s <puerto>\n", pointArg[0] );
		exit( 1 );
	}

	sockfd = socket( AF_INET, SOCK_STREAM, 0);
	if ( sockfd < 0 ) { 
		perror( " apertura de socket ");
		exit( 1 );
	}

	memset( (char *) &serv_addr, 0, sizeof(serv_addr) );
	puerto = atoi( pointArg[1] );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );

	if ( bind(sockfd, ( struct sockaddr *) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
		perror( "ligadura" );
		exit( 1 );
	}

        printf( "Proceso serverTCP: %d - socket disponible: %d\n", getpid(), ntohs(serv_addr.sin_port) );

	listen( sockfd, 5 );
	clilen = sizeof( cli_addr );

	while( 1 ) {
		newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );
		if ( newsockfd < 0 ) {
			perror( "accept" );
			exit( 1 );
		}

		pid = fork(); 
		if ( pid < 0 ) {
			perror( "fork" );
			exit( 1 );
		}

		if ( pid == 0 ) {  // Proceso hijo
			close( sockfd );

			while ( 1 ) {
				memset( buffer, 0, TAM );

				n = read( newsockfd, buffer, TAM-1 );// <=========== **** READ ****
				if ( n < 0 ) {
					perror( "lectura de socket" );
					exit(1);
				}

				printf( "PROCESO %d. ", getpid() );
				printf( "Recibí por TCP: %s", buffer );

				// Verificación de si hay que terminar
				buffer[strlen(buffer)-1] = '\0';
					if( !strcmp( "disconnect", buffer ) ) {
					n = write( newsockfd, "Desconectado.", 18 ); // <=========== **** WRITE **** (a ser cambiado por "send" para envio bloqueante)
					if ( n < 0 ) {
						perror( "escritura en socket" );
						exit( 1 );
					}					
					printf( "PROCESO %d. Como recibí 'disconnect', termino la ejecución.\n\n", getpid() );
					exit(0);
				} else if( !strcmp( "erase_data", buffer ) ) {
                    eraseData(sem);
                    n = write( newsockfd, "El registro fue eliminado correctamente.", 40 );
                    if ( n < 0 ) {
                        perror( "escritura en socket" );
                        exit( 1 );
                    }				
				} else {
					n = write( newsockfd, "Comando ejecutado.", 18 ); // <=========== **** WRITE **** (a ser cambiado por "send" para envio bloqueante)
					if ( n < 0 ) {
						perror( "escritura en socket" );
						exit( 1 );
					}					
				}
			}
		}
		else {
			printf( "SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid );
			close( newsockfd );
		}
	}
	return 0; 
} 