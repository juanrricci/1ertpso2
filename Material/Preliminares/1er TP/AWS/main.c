#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <semaphore.h> // POSIX semaphores 
#include <fcntl.h> // semaphore flags: O_* constants 
#include <sys/stat.h> // semaphore modes: S_* constants 

#include "./headers/captureData.h"
#include "./headers/serverTCP.h"
#include "./headers/serverUDP.h"

int main(int argc, char *argv[]) {

	/*
	Elimininacion del archivo DATA que pueda quedar
	de una ejecucion anterior
	*/ 
	system("rm ./files/data.txt");

	/*
	Creacion del semaforo necesario para el acceso a 
	DATA
	*/
	sem_t *sem = sem_open( "/MyTestSemaphore", O_CREAT | // create if it does not already exist 
	O_CLOEXEC , // close on execute 
	S_IRUSR | // user permission: read 
	S_IWUSR , // user permission: write 
	1 ); // initial value of the semaphore

	/*
	Primer fork para generar el "capturador de datos"
	desde los sensores
	*/
	int pidCD = fork();
	if(pidCD >= 0) { // fork realizado correctamente
			if(pidCD == 0) {
				// Hijo que ejecuta la funcion de manejo de archivos
				//printf("PID de captureData: %d\n", getpid());
				captureData(argc, argv, sem);
			}
	}
	
	/*
	Segundo fork para levantar el servidor TCP
	*/
	int pidTCP = fork();
	if(pidTCP >= 0) { // fork realizado correctamente
			if(pidTCP == 0) {
				//printf("Fork de serverTCP: %d\n", getpid());
				serverTCP(argc, argv, sem);
			}
	}

	/* 
	El proceso padre ejecuta el servidor UDP
	*/
	//printf("Proceso serverUDP: %d\n", getpid());
	serverUDP(argc, argv, sem);

    return 0;
}

