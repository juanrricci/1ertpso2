/**
 * @file
 * @brief Solicita credenciales de acceso.
 * @param[out] String con credenciales ingresadas
 *
 * Solicita credenciales de acceso en formato "connect usuario@numero_ip:puerto"
 */
void login( char *credenciales );