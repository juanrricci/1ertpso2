/**
 * @file
 * @brief Conecta con el servidor mediante socket TCP.
 * @param[in] String con nombre de usuario.
 * @param[in] String con dirección IP.
 * @param[in] String con número de puerto.
 * @return int con el file descriptor del socket TCP.
 *
 * Conexión con el servidor mediante socket TCP. Emplea los datos de dirección
 * IP y puerto. Si estos datos son validados, se envía el nombre de usuario al
 * servidor, el cual también debe ser validado. Luego, se solicita al usuario
 * que ingrese su contraseña, la cual también es validada por el servidor.
 * @debug Se pueden generar sub-funciones en esta parte.
 */
int conectar_via_TCP( char *usuario, char *ip, char *puerto );