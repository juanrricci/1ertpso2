/**
 * @file
 * @brief Valida las credenciales de acceso
 * @param[in] String con credenciales ingresadas
 *
 * Parsea las credenciales y valida una por una.
 * Llama a la función "conectar_via_tcp" para validar dirección IP, puerto,
 * nombre de usuario y contraseña contra el servidor de AWS.
 */
int validacion( char *credenciales );