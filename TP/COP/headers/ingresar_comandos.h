/**
 * @file
 * @brief Muestra prompt para ingreso de comandos.
 * @param[in] String con credenciales ingresadas.
 * @param[in] int file descriptor del socket TCP.
 *
 * Genera prompt con el formato "usuario@host >" y queda a la espera de que el
 * usuario ingrese un comando.
 * @debug Debería hacerse una función por separado que cree el prompt.
 */
void ingresar_comandos( char* credenciales, int sockfd );