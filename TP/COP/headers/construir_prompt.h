/** 
 * @file
 * @brief Construcción del prompt
 * @param[in] String con las credenciales ingresadas.
 * @param[out] String con el prompt.
 *
 * Se concatena el nombre de usuario con el host en formato "usuario@host >"
 */
void construir_prompt( char *credenciales, char *prompt );