/**
 * @file
 * @brief Cliente para consultas a servidor de AWS.
 */

// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Librerías de sockets
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define TAM 256 //< Constante de tamaño de buffer de comunicación

// Headers
#include "./headers/login.h"
#include "./headers/validacion.h"
#include "./headers/menu.h"
#include "./headers/ingresar_comandos.h"

void main( int argc, char *argv[] ) 
{
    // Variables
    char credenciales[ TAM ]; /*< Variable donde se almacenan las credenciales
                               * en el login del sistema
                               */
    int sockfd = 0; //< Flag de validación de credenciales de login

    memset( credenciales, 0, sizeof( credenciales )); //< Limpieza del buffer de credenciales

    // cleardata();
    // system("rm ./files/rcvdData.txt");

    // Título del programa
    printf( "\n*** COP SERVICIO METEOROLOGICO NACIONAL ***" );
    printf( "\n    -----------------------------------    \n\n" );
    
    while( !sockfd )
    {
        // Se llama a la función de login y se pasan las credenciales que el programa recibe en la variable global "credenciales".
        login( credenciales );
        // Si la validación es aceptada, entonces validado se pone en 1 y se permite el acceso al menú de opciones.
        sockfd = validacion( credenciales );
    }

    // GENERAR SOCKET UDP
    // int sockfd_udp = conectar_via_udp( credenciales, &dest_addr, &tamano_direccion );
    // ***************
    menu( );
    ingresar_comandos( credenciales, sockfd );
}

/* ORGANIZACION DEL TRABAJO: 2DA ITERACION
12) codear segun https://www.gnu.org/prep/standards/html_node/Writing-C.html
13) probar que funcione en debian recien instalado y sin conexion a internet ,y en la raspberry (ponerle ip fija a ambas)
*/