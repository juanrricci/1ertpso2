// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Headers de funciones propias del programa
#include "../headers/menu.h"

void menu( )
{
	printf( "BIENVENIDO. Por favor, ingrese alguno de los siguientes comandos:\n\n" );

    printf( "listar: muestra un listado de todas las estaciones que hay en la ");
    printf(	"“base de datos” y muestra de que sensores tiene datos.\n\n" );

    printf( "descargar no_estación: descarga un archivo con todos los datos de ");
	printf( "no_estación.\n\n" );

    printf( "diario_precipitacion no_estación: muestra el acumulado diario de la ");
    printf( "variable precipitación de no_estación (no_día: acumnulado mm).\n\n" );

    printf( "mensual_precipitacion no_estación: muestra el acumulado mensual de la ");
    printf(" variable precipitación (no_día: acumnulado mm).\n\n" );

    printf( "promedio variable: muestra el promedio de todas las muestras de la ");
    printf( "variable de cada estación (no_estacion: promedio).\n\n" );

    printf( "desconectar: termina la sesión del usuario.\n\n" );
}