// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

// Headers
#include "../headers/ingresar_comandos.h"
#include "../headers/construir_prompt.h"
#include "../headers/escribir_tcp.h"
#include "../headers/leer_tcp.h"
#include "../headers/descargar_udp.h"

#define TAM 10000

void ingresar_comandos( char* credenciales, int sockfd )
{
	char comando[ TAM ];

	char comando_copia[ TAM ];
	char respuesta[ TAM ];
	char prompt[ TAM ];
	memset( prompt, '\0', TAM );

	char credenciales_copia[ TAM ];
	memset( credenciales_copia, '\0', TAM );
	strcpy( credenciales_copia, credenciales );

	construir_prompt( credenciales, prompt ); //< Construcción del prompt

	int n;
	int desconectar = 0;
	char* parseo;
	
	while( 1 ) //< Bucle de ingreso de comandos
	{
		memset( comando, '\0', TAM );
		memset( comando_copia, '\0', TAM );

		printf( "%s", prompt );
		fgets( comando, TAM - 1, stdin );	

		strcpy( comando_copia, comando );
		parseo = strtok( comando_copia, " " );

		// Se levanta un flag en caso de haber ingresado "desconectar"
		if( !strcmp( parseo, "desconectar\n" ))
		{
			desconectar = 1;
		}

		// Se chequea si el comando ingresado es "descargar". En ese caso,
		// se envía el comando por TCP y la recepción de los datos a guardar
		// en archivo se realiza por UDP, hasta recibir un mensaje de "fin",
		// que es cuando se finaliza la recepción.
		if( !strcmp( parseo, "descargar" ))
		{
			n = escribir_tcp( sockfd, comando );			
			descargar_udp( credenciales_copia );	
		}

		// Si el comando es cualquier otro, se envía por TCP y se habilita
		// un bucle de recepcion por TCP hasta que se recibe un mensaje de
		// "fin", y se finaliza la recepción.
		else
		{
			// Escritura en el socket TCP 
			n = escribir_tcp( sockfd, comando );

			memset( comando, '\0', TAM );
			
			while( 1 ) //< Bucle de recepción de datos mediante "read"
			{
				memset( comando, '\0', TAM );

				n = leer_tcp( sockfd, comando );

				/* 
				 * Si el dato recibido NO es "fin", entonces se imprime en pantalla
				 * el dato recibido.
				 */
				if( strcmp( comando, "fin\n" ))
				{
					printf( "%s\n", comando );	
				}
				/* 
				 * Si el dato recibido es "fin", entonces el envío de datos finalizó
				 * y se sale del bucle de recepción de datos.
				 */
				else
				{
					break;
				}
			}			
		}

		// Si el flag de desconexion está levantado, se cierra el programa cliente.
		if( desconectar )
		{
			printf( "Desconectado del servidor de AWS.\n" );
			exit( 0 );
		}
	}
}