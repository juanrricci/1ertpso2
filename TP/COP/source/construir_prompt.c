// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Headers
#include "../headers/construir_prompt.h"

void construir_prompt( char *credenciales, char *prompt )
{
	char *parseo = strtok( credenciales, " " );
	parseo = strtok( NULL, "@" );
	strcpy( prompt, parseo );
	strcat( prompt, "@" );
	parseo = strtok( NULL, ":" );
	strcat( prompt, parseo );
	strcat( prompt," > " );
}