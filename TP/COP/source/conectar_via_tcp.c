// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h>
#include <termios.h>

// Headers
#include "../headers/conectar_via_tcp.h"
#include "../headers/escribir_tcp.h"
#include "../headers/leer_tcp.h"

#define TAM 256

int conectar_via_TCP( char *usuario, char *ip, char *puerto )
{
	struct termios oflags, nflags;
	int sockfd, puerto_int, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	int terminar = 0;
	char buffer[ TAM ];

	sockfd = socket( AF_INET, SOCK_STREAM, 0 );	/*< Generación del file descriptor
												 *  del socket TCP.
												 */
	if( sockfd < 0 ) 
	{
		perror( "ERROR apertura de socket" );
		return 0;
	}

	server = gethostbyname( ip ); //< Chequeo de la IP ingresada.
	if( server == NULL ) 
	{
		fprintf( stderr, "Error, no existe el host\n" );
		return 0;
	}

	else
	{
		printf( "Host validado.\n" );
	}

	// Conexión con el servidor.
	memset( (char *)&serv_addr, '0', sizeof( serv_addr ));
	serv_addr.sin_family = AF_INET;
	bcopy( (char *)server -> h_addr, (char *)&serv_addr.sin_addr.s_addr, server -> h_length );
	puerto_int = atoi( puerto );
	serv_addr.sin_port = htons( puerto_int );
	if ( connect( sockfd, (struct sockaddr *)&serv_addr, sizeof( serv_addr )) < 0 ) 
	{
		perror( "Puerto no valido" );
		return 0;
	}

	//***************************************************************************************
	//***************************************************************************************
	// Envío del nombre de usuario al servidor.
	memset( buffer, '\0', TAM );
	strcpy( buffer, usuario );
	strcat( buffer, "\n" );
	printf( "Se envía nombre de usuario: %s\n", usuario );

	n = escribir_tcp( sockfd, buffer );

	// Recepción de la respuesta por parte del servidor.
	memset( buffer, '\0', TAM );
	n = leer_tcp( sockfd, buffer );

	printf( "Respuesta: %s\n", buffer );

    // Si el nombre de usuario no es válido, se retorna credencial invalida
	if( strcmp( buffer, "Nombre de usuario valido" )) 
	{
		return 0;
	}

	//***************************************************************************************
	//***************************************************************************************
	// Se solicita contraseña y se la envía al servidor.
	/* disabling echo */
    tcgetattr( fileno( stdin ), &oflags );
    nflags = oflags;
    nflags.c_lflag &= ~ECHO;
    nflags.c_lflag |= ECHONL;

    if( tcsetattr( fileno( stdin ), TCSANOW, &nflags) != 0 ) 
    {
        perror( "tcsetattr" );
        return EXIT_FAILURE;
    }

	printf( "Ingrese la contraseña: " );
	memset( buffer, '\0', TAM );
	fgets( buffer, TAM-1, stdin );

	n = escribir_tcp( sockfd, buffer );
	
	/* restore terminal */
    if( tcsetattr( fileno( stdin ), TCSANOW, &oflags) != 0) 
    {
        perror( "tcsetattr" );
        return EXIT_FAILURE;
    }

	// Recepción de la respuesta por parte del servidor.
	memset( buffer, '\0', TAM );
	n = leer_tcp( sockfd, buffer );
	
	printf( "Respuesta: %s\n", buffer );

	// Si la contraseña no es válida, se retorna credencial invalida
	if( strcmp( buffer, "Contraseña valida" )) 
	{
		return 0;
	}	

	// Se retorna el numero de socket para poder seguir realizando envíos al servidor.
	return sockfd;
}
