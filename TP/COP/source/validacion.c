// Librerías estándar
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Headers
#include "../headers/validacion.h"
#include "../headers/conectar_via_tcp.h"

int validacion( char *credenciales_originales ) 
{
    char credenciales[ 256 ];
    memset( credenciales, 0, sizeof( credenciales ));
    strcpy( credenciales, credenciales_originales );
    char connect_ok[ ] = "connect"; 

    // Parseo de las credenciales mediante "strtok"
    char *connect = strtok( credenciales," " );

    // Se retorna la validación de las credenciales mediante su comparación con las correctas.
    if( !strcmp( connect, connect_ok ))
    {
        char *usuario = strtok( NULL, "@" );
        char *ip = strtok( NULL, ":" );
        char *puerto = strtok( NULL, " \n\t" );

        int sockfd = conectar_via_TCP( usuario, ip, puerto );
        /*
         * Si la conexión TCP es validada (mediante host, puerto ,usuario y contraseña),
         * se retorna su file descriptor para poder enviar comandos. 
         * De lo contrario, se retorna cero.
         */

        if( sockfd )
        {
            printf( "Conexion aceptada.\n\n" );
            return sockfd;
        }
        else
        {
            printf( "Credenciales no validas. Intente nuevamente.\n\n" );
            return 0;
        }
    }
    else
    {        
        printf( "Falta la palabra 'connect'.\n" );
        printf( "Credenciales no validas. Intente nuevamente.\n\n" );
        return 0;
    }
}
