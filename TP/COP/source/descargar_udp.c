#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#include "../headers/descargar_udp.h"

#define TAM 256

void descargar_udp( char* credenciales ) 
{
	char credenciales_copia[ TAM ];
    memset( credenciales_copia, '\0', sizeof( credenciales_copia ));
    strcpy( credenciales_copia, credenciales );

	int sockfd, n, tamano_direccion;
	struct sockaddr_in dest_addr;
	struct hostent *server;

	// Parseo de las credenciales
	char* connect = strtok( credenciales_copia," " );
    char* usuario = strtok( NULL, "@" );
    char* ip = strtok( NULL, ":" );
    char* puerto = strtok( NULL, " \n\t" );

	server = gethostbyname( ip );
	if ( server == NULL ) 
	{
		fprintf( stderr, "ERROR, no existe el host\n" );
		exit( 0 );
	}

	int puerto_int = atoi( puerto );

	sockfd = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd < 0) 
	{
		perror( "apertura de socket" );
		exit( 1 );
	}

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons( atoi( puerto ) );
	dest_addr.sin_addr = *( (struct in_addr *)server->h_addr );
	memset( &(dest_addr.sin_zero), '\0', 8 );

	tamano_direccion = sizeof( dest_addr );

	char respuesta[ TAM ];

	// Primer envio hacia el servidor para establecer comunicacion udp.
	n = sendto( sockfd, (void *)"hola", TAM, 0, (struct sockaddr *)&dest_addr, tamano_direccion );
	if ( n < 0 ) {
		perror( "Escritura en socket" );
		exit( 1 );
	}

	FILE* datos_recibidos = fopen( "datos_recibidos", "w" );
	// Bucle de recepción de datos
	while( 1 ) //< Bucle de recepción de datos mediante "read"
	{
		// ACA VA TODA LA OPERACION CON UDP
		memset( respuesta, '\0', TAM );

		n = recvfrom( sockfd, (void *)respuesta, TAM, 0, (struct sockaddr *)&dest_addr, &tamano_direccion );

		if ( n < 0 ) {
			perror( "Lectura de socket" );
			exit( 1 );
		}

		/* 
		 * Si el dato recibido NO es "fin", entonces se imprime en pantalla
		 * el dato recibido.
		 */
		if( strcmp( respuesta, "fin\n" ))
		{
			fputs( respuesta, datos_recibidos );
		}
		/* 
		 * Si el dato recibido es "fin", entonces el envío de datos finalizó
		 * y se sale del bucle de recepción de datos.
		 */
		else
		{
			break;
		}
	}		

	fclose( datos_recibidos );

	close( sockfd );		
} 