#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "../headers/escribir_tcp.h"

#define TAM 256

int escribir_tcp( int newsockfd, char* string )
{
	int n = write( newsockfd, string, TAM-1 );
	if ( n < 0 ) {
		perror( "escritura en socket" );
		exit( 1 );
	}	

	return n;
}