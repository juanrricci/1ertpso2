/**
  * @file
  * @brief Divide la información de "datos_metASCII.csv" por estación.
  *
  * Genera archivos cuyos nombres son los números de las estaciones.
  * Almacena en cada uno de ellos la información correspondiente a la
  * respectiva estación.
  */
void parseo_general( );