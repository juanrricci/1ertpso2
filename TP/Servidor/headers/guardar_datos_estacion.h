/**
  * @file
  * @brief Guardar datos de cada estación en archivos separados.
  *
  * Genera un archivo llamado "nros_estaciones" donde guarda los números relevados
  * de estaciones. Luego, genera archivos titulados con los números de estaciones
  * y guarda en ellos los datos correspondientes a cada estación.
  */
void guardar_datos_estacion( int contador_linea, char* linea );