/**
  * @file
  * @brief Guarda los títulos de las variables meteorológicas
  *
  * Esta función parsea los datos de la línea 3 del archivo "datos_metASCII:csv"
  * los cuales corresponden a los nombres de las variables meteorológicas.
  * Estos son guardados en un archivo denominado "titulos".
  */

void guardar_titulos(char* linea);