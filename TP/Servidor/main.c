/**
 * @file
 * @brief Servidor de AWS.
 *
 * Valida conexión de clientes, recibe comandos y responde enviando los datos
 * parseados del archivo "datos_metASCII.txt".
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "./headers/conectar.h"
#include "./headers/parseo_general.h"
#include "./headers/listar.h"
#include "./headers/descargar.h"
#include "./headers/diario_precipitacion.h"
#include "./headers/mensual_precipitacion.h"
#include "./headers/promedio_variable.h"

void main( )
{
	// Llamado a comandos de sistema: remueven los archivos generados en corridas anteriores.
	system( "find . -maxdepth 1 -type f | grep -v -e main.c -e makefile -e datos_metASCII.csv -e servidor | xargs rm" );

	// Generación de archivos con números de estaciones, y con datos separados por número de estación.
	parseo_general( );
	
	// Generación de socket TCP y UDP para interacción con los clientes.
	conectar( );
}