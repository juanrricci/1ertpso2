#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/validar_comando.h"

#define TAM 256

int validar_comando( char* parseo )
{
	if( !strcmp( parseo, "listar\n" ))
	{
		return 1;
	}

	else if( !strcmp( parseo, "descargar" ))
	{
		return 2;
	}

	else if( !strcmp( parseo, "diario_precipitacion" ))
	{
		return 3;
	}

	else if( !strcmp( parseo, "mensual_precipitacion" ))
	{
		return 4;
	}

	else if( !strcmp( parseo, "promedio" ))
	{
		return 5;
	}

	else if( !strcmp( parseo, "desconectar\n" ))
	{
		return 6;
	}
	
	else
	{
		return 0;
	}
}