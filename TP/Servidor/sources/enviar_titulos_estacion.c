#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "../headers/enviar_titulos_estacion.h"
#include "../headers/escribir_tcp.h"

#define TAM 200

void enviar_titulos_estacion( int newsockfd, char* nro_estacion )
{
	char titulo_estacion[ TAM ];
	memset( titulo_estacion, '\0', sizeof( titulo_estacion ));  

    char titulo_variable[ TAM ];
	memset( titulo_variable, '\0', sizeof( titulo_variable ));  

	char* titulo_parseado;  

	escribir_tcp( newsockfd, "" );
	strcpy( titulo_estacion, "SENSORES DE LA ESTACION NRO " );
	strcat( titulo_estacion, nro_estacion );
	strcat( titulo_estacion, ":" );
	escribir_tcp( newsockfd, titulo_estacion );

	// Lectura del archivo donde se almacenan los títulos de las variables
	// de la estación.
	FILE* titulos_estacion = fopen( strcat( nro_estacion, "_titulos" ), "r" );

	while( fgets( titulo_variable, TAM, ( FILE* )titulos_estacion) != NULL )
	{
		titulo_parseado = strtok( titulo_variable, "\n" );
		escribir_tcp( newsockfd, titulo_parseado );
		memset( titulo_variable, '\0', sizeof( titulo_variable ));    
	}

	escribir_tcp( newsockfd, "" );

	// Se cierran todos los archivos utilizados.
	fclose(titulos_estacion);
}