#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "../headers/generar_socket_tcp.h"

int generar_socket_tcp( int* clilen, struct sockaddr* cli_addr )
{
	int sockfd, puerto;
	struct sockaddr_in serv_addr;
	int n;

	// Número de puerto de comunicación.
	puerto = 6020;

	// Generación del file descriptor del socket TCP.
	sockfd = socket( AF_INET, SOCK_STREAM, 0 );
	if( sockfd < 0 ) 
	{ 
		perror( " apertura de socket ");
		exit( 1 );
	}

	// Esta línea permite reutilizar inmediatamente el número de puerto del 
	// servidor luego de su desconexión.
	if( setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0 )
	{
    	perror("setsockopt(SO_REUSEADDR) failed");
	}

	// Generación de la ligadura (conexión disponible para los clientes) 
	memset( (char *) &serv_addr, 0, sizeof(serv_addr) );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );

	if ( bind(sockfd, ( struct sockaddr *)&serv_addr, sizeof( serv_addr )) < 0 ) 
	{
		perror( "ligadura" );
		exit( 1 );
	}

    printf( "Proceso: %d - socket TCP disponible: %d\n", getpid( ), ntohs( serv_addr.sin_port ));
	listen( sockfd, 5 );
	*clilen = sizeof( cli_addr );

	return sockfd;
}
