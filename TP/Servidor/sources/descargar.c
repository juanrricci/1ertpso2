#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "../headers/descargar.h"
#include "../headers/validar_estacion.h"

#define TAM 256

void descargar( char* nro_estacion )
{
	// GENERACION DEL SOCKET UDP
	int puerto = 6020;
	int n;	
	struct sockaddr_in serv_addr;

	int sockfd = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd < 0) 
	{ 
		perror("ERROR en apertura de socket");
		exit( 1 );
	}


	// Esta línea permite reutilizar inmediatamente el número de puerto del 
	// servidor luego de su desconexión.
	if( setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0 )
	{
    	perror("setsockopt(SO_REUSEADDR) failed");
	}

	memset( &serv_addr, 0, sizeof(serv_addr) );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );
	memset( &(serv_addr.sin_zero), '\0', 8 );

	if( bind( sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0 ) 
	{
		perror( "ERROR en binding" );
		exit( 1 );
	}

        printf( "Socket UDP disponible: %d\n", ntohs(serv_addr.sin_port) );

	int tamano_direccion = sizeof( struct sockaddr );

	// Primera recepción por parte del servidor para establecer comunicacion udp con cliente
	char buffer[ TAM ];
	memset( buffer, '\0', TAM );

	n = recvfrom( sockfd, buffer, TAM-1, 0, (struct sockaddr *)&serv_addr, &tamano_direccion );
	if ( n < 0 ) {
		perror( "lectura de socket" );
		exit( 1 );
	}

	// VALIDACION DEL NUMERO DE ESTACION INGRESADO
	if( !validar_estacion( nro_estacion ))
	{
		for( int i = 0; i < 65530; i++ ) {}
		n = sendto( sockfd, (void *)"Estacion NO válida.\n", TAM, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
		if ( n < 0 ) 
		{
			perror( "escritura en socket" );
			exit( 1 );
		}

		n = sendto( sockfd, (void *)"fin\n", TAM, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
		if ( n < 0 ) 
		{
			perror( "escritura en socket" );
			exit( 1 );
		}

		return;
	}


	// ENVIO DE MENSAJES POR UDP (si la estacion es validada)
	FILE* datos_estacion = fopen( nro_estacion, "r" );

	memset( buffer, '\0', TAM );

	while( fgets( buffer, TAM, ( FILE* ) datos_estacion ))
	{
		for( int i = 0; i < 65530; i++ ) {}
		n = sendto( sockfd, (void *)buffer, TAM, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
		if ( n < 0 ) 
		{
			perror( "escritura en socket" );
			exit( 1 );
		}
	}

	for( int i = 0; i < 65530; i++ ) {}
	n = sendto( sockfd, (void *)"fin\n", TAM, 0, (struct sockaddr *)&serv_addr, tamano_direccion  );
	if ( n < 0 ) 
	{
		perror( "escritura en socket" );
		exit( 1 );
	}

	fclose( datos_estacion );
	
	close( sockfd );
}