#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "../headers/hijo.h"
#include "../headers/recepcion_usuario.h"
#include "../headers/recepcion_contrasena.h"
#include "../headers/leer_tcp.h"
#include "../headers/validar_comando.h"
#include "../headers/devolver_respuesta.h"

#define TAM 256

void hijo( int sockfd, int newsockfd )
{
	char* parseo;
	int n, opcion_comando;

	char comando[ TAM ];
	memset( comando, '\0', TAM );
	
	char copia_comando[ TAM ];
	memset( copia_comando, '\0', TAM );

	close( sockfd );

	recepcion_usuario( newsockfd );
	recepcion_contrasena ( newsockfd );


	// Bucle de recepción de comandos.
	while ( 1 ) 
	{
		n = leer_tcp( newsockfd, comando );

		printf( "PROCESO %d. ", getpid() );
		printf( "Recibí: %s", comando );

		strcpy( copia_comando, comando );

		// En "copia_comando" se busca el token " " (espacio en blanco).
		// En caso de no ser encontrado, se parseará la totalidad del comando.
		// En caso de ser encontrado, se parseará el comando hasta ese punto
		// lo cual es conveniente para los comandos de más de un argumento.
		parseo = strtok( copia_comando, " " );

		opcion_comando = validar_comando( parseo );
		
		devolver_respuesta( opcion_comando, newsockfd, comando );
	}
}