#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/parseo_general.h"
#include "../headers/guardar_titulos.h"
#include "../headers/guardar_datos_estacion.h"

void parseo_general( )
{
	// VARIABLES:
	// String que almacena cada línea extraída de "datos_meteorologicos".
	char linea[ 1000 ];
    memset( linea, '\0', sizeof( linea ));

    // Contador de líneas de "datos_meteorologicos". Sirve para discriminar
    // ciertas líneas que contienen datos no relevantes.
    int contador_linea = 0;

    // Puntero al archivo "datos_meteorologicos".
	FILE* completo = fopen( "datos_metASCII.csv", "r" );

    // Bucle que recorre el archivo "datos_meteorologicos" hasta el final,
    // extrae cada línea y la almacena en distintos archivos.
	while( fgets( linea, 1000, ( FILE* )completo) != NULL )
    {
    	contador_linea++;
    	if( contador_linea == 3) //< Linea que contiene los títulos de las variables
    	{
    		// Se guardan los títulos de las variables el archivo "titulos".
            guardar_titulos( linea );
    	}
        else
        {
            // Ejecuta la función que separa los datos del archivo "datos_metASCII.csv"
            // en varios archivos con los datos propios de cada estación.
            guardar_datos_estacion( contador_linea, linea );
        }

        memset( linea, '\0', sizeof( linea )); //< Limpia el buffer "linea" antes
                                               //  de extraer la próxima línea.
    }

    fclose( completo );
}
