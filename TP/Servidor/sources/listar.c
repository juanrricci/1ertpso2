#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/listar.h"
#include "../headers/guardar_titulos_estacion.h"
#include "../headers/enviar_titulos_estacion.h"

#define TAM 200

void listar( int newsockfd )
{
	char nro_estacion[ TAM ];
    memset( nro_estacion, '\0', sizeof( nro_estacion ));

    char* nro_estacion_parseado;

    // Apertura del archivo con los números de estaciones.
	FILE* nros_estaciones = fopen( "nros_estaciones", "r" );

	// Bucle que extrae cada número de estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que guarda los títulos de las variables
		// de cada estación en archivos separados.
		guardar_titulos_estacion( nro_estacion_parseado );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}

	rewind( nros_estaciones );

	// Bucle que envía los títulos de las variables de cada estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que envía los títulos de las variables
		// de cada estación.
		enviar_titulos_estacion( newsockfd, nro_estacion_parseado );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}

	fclose(nros_estaciones);

}