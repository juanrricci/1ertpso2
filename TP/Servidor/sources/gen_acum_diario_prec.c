#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/gen_acum_diario_prec.h"
#include "../headers/gen_archivo_prec.h"

#define TAM 200

void gen_acum_diario_prec( char* nro_estacion )
{
	gen_archivo_prec( nro_estacion );

	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	char linea[ TAM ];
	memset( linea ,'\0', sizeof( linea ));

	char dia_anterior[ TAM ];
	memset( dia_anterior, '\0', sizeof( dia_anterior ));

	char dia_siguiente[ TAM ];
	memset( dia_siguiente, '\0', sizeof( dia_siguiente ));

	char acumulado_char[ TAM ];
	memset( acumulado_char, '\0', sizeof( acumulado_char ));

	double acumulado_double;

	// Apertura del archivo con los valores de precipitaciones de la estacion.
	strcpy( nro_estacion_copia, nro_estacion );
	FILE* archivo_precipitaciones = fopen( strcat( nro_estacion_copia, "_precipitaciones" ), "r" );

	// Apertura del archivo donde se almacena el acumulado diario.
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acumulado_diario = fopen( strcat( nro_estacion_copia, "_acum_diario" ), "w" );

	// Relevo del primer día almacenado en el archivo de precipitaciones.
	fgets( linea, TAM,( FILE* )archivo_precipitaciones );
	char* parseo = strtok( linea, " " );
	strcpy( dia_anterior, parseo );

	rewind( archivo_precipitaciones );

	// Bucle de acumulacion diaria de precipitaciones
	while( fgets( linea, TAM,( FILE* )archivo_precipitaciones ) != NULL)
	{
		parseo = strtok( linea, " " );
		strcpy( dia_siguiente, parseo );

		// Si el dia relevado anteriormente es distinto del relevado en el paso 
		// actual, se almacena el acumulado diario de precipitacion
		if( strcmp( dia_anterior, dia_siguiente ))
		{
			strcat( dia_anterior, ": " );

			// Casteo del valor double a string y guardado en acumulado_char
			sprintf( acumulado_char, "%g", acumulado_double );
			strcat( dia_anterior, acumulado_char );
			strcat( dia_anterior, "\n" );

			// Guardado del acumulado en el archivo.
			fputs( dia_anterior, acumulado_diario );

			// Actualizacion del "dia anterior" y borrado del double acumulado.			
			strcpy( dia_anterior, dia_siguiente );
			acumulado_double=0.0;
		}

		// Se parsea el valor de precipitacion y se lo acumula en el double 
		// acumulado.
		parseo = strtok( NULL, "," );
		parseo = strtok( NULL, "\n" );
		acumulado_double += atof( parseo );
	}

	// Se repiten los pasos para almacenar el último acumulado
	strcat( dia_anterior, ": " );
	sprintf( acumulado_char, "%g", acumulado_double );
	strcat( dia_anterior, acumulado_char );
	fputs(dia_anterior,acumulado_diario);

	// Se cierran los archivos utilizados.
	fclose(archivo_precipitaciones);
	fclose(acumulado_diario);
}