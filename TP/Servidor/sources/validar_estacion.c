#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/validar_estacion.h"

#define TAM 200

int validar_estacion( char* nro_estacion )
{	
	char nro_estacion_archivo[ TAM ];
    memset( nro_estacion_archivo, '\0', sizeof( nro_estacion_archivo ));

    char* parseo;
    int estacion_encontrada = 0;

	FILE* nros_estaciones = fopen( "nros_estaciones", "r" );
	while( fgets( nro_estacion_archivo, TAM, ( FILE* )nros_estaciones ) != NULL )
	{
		parseo = strtok( nro_estacion_archivo, "\n" );
		if( !strcmp( nro_estacion, parseo ))
		{
			estacion_encontrada = 1;
			break;
		}
	}

	fclose( nros_estaciones );

	return estacion_encontrada;
}