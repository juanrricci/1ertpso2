#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/mensual_precipitacion.h"
#include "../headers/validar_estacion.h"
#include "../headers/escribir_tcp.h"
#include "../headers/gen_acum_mensual_prec.h"

#define TAM 200

void mensual_precipitacion( int newsockfd, char *nro_estacion )
{
	if( !validar_estacion( nro_estacion ))
	{
		escribir_tcp( newsockfd, "Estacion NO válida\n" );
		return;
	}

	gen_acum_mensual_prec( nro_estacion );

	char linea[ TAM ];
	memset( linea ,'\0', sizeof( linea ));

	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acumulado_mensual = fopen( strcat( nro_estacion_copia, "_acum_mensual" ), "r" );

	int n = escribir_tcp( newsockfd, "" );

	while( fgets( linea, TAM,( FILE* )acumulado_mensual ) != NULL)
	{
		char* parseo = strtok( linea, "\n" );
		n = escribir_tcp( newsockfd, parseo );
	}

	n = escribir_tcp( newsockfd, "" );
}