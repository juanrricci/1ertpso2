#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/recepcion_usuario.h"
#include "../headers/leer_tcp.h"
#include "../headers/escribir_tcp.h"

#define TAM 256

void recepcion_usuario( int newsockfd )
{
	char usuario[ TAM ];
	int n = leer_tcp( newsockfd, usuario );

	printf( "PROCESO %d. ", getpid() );
	printf( "Recibí nombre de usuario: %s", usuario );

	if( strcmp( usuario, "juan\n" ))
	{
		n = escribir_tcp( newsockfd, "Nombre de usuario invalido" );		
	}

	else
	{
		n = escribir_tcp( newsockfd, "Nombre de usuario valido" );				
	}	
}

