#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "../headers/devolver_respuesta.h"
#include "../headers/listar.h"
#include "../headers/descargar.h"
#include "../headers/diario_precipitacion.h"
#include "../headers/mensual_precipitacion.h"
#include "../headers/promedio_variable.h"
#include "../headers/escribir_tcp.h"

#define TAM 256

void devolver_respuesta( int opcion_comando, int newsockfd, char* comando )
{
	int n;
	char* parseo;

	switch( opcion_comando )
	{
		case 1:
			listar( newsockfd );
			break;

		case 2: //> enviar_descarga(sockUDP);
			parseo = strtok( comando, " " );
			parseo = strtok( NULL, "\n" );
			descargar( parseo );
			break;					

		case 3:
			parseo = strtok( comando, " " );
			parseo = strtok( NULL, "\n" );
			diario_precipitacion( newsockfd, parseo );
			break;		

		case 4:
			parseo = strtok( comando, " " );
			parseo = strtok( NULL, "\n" );
			mensual_precipitacion( newsockfd, parseo );						
			break;	

		case 5:
			parseo = strtok( comando, " " );
			parseo = strtok( NULL, "\n" );
			promedio_variable( newsockfd, parseo );
			break;			

		case 6:
			printf( "PROCESO %d. Como recibí 'desconectar', termino la ejecución.\n\n", getpid() );
			n = escribir_tcp( newsockfd, "fin\n" );
			exit(0);
			break;			

		default:
			n = escribir_tcp( newsockfd, "Recibi un comando desconocido\n" );			
	}

	if( opcion_comando != 2 ) //> El comando 2 manda "fin" por UDP.
	{
		n = escribir_tcp( newsockfd, "fin\n" );
	}
}