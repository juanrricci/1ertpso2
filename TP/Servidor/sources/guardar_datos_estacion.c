#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/guardar_datos_estacion.h"

void guardar_datos_estacion( int contador_linea, char* linea )
{
	// VARIABLES:
	// String que almacena cada línea extraída de "datos_meteorologicos".
	char linea_copia[ 1000 ];
    memset( linea_copia, '\0', sizeof( linea_copia ));    

	FILE* datos_estacion;
	FILE* nros_estaciones;
	
	char* dato_linea;

    // variables para comparar los numeros de las estaciones, para reconocer 
    // cuando el dato corresponde a otra estacion y debe ser almacenado en 
    // un nuevo archivo.
	char nro_estacion_ant[ 8 ];
    memset( nro_estacion_ant, '\0', sizeof( nro_estacion_ant ));

	char nro_estacion_sig[ 8 ];
    memset( nro_estacion_sig, '\0', sizeof( nro_estacion_sig ));

    char nro_estacion_save[ 8 ];
    memset( nro_estacion_save, '\0', sizeof( nro_estacion_save ));

    char nro_estacion_ant2[ 8 ];
    memset( nro_estacion_ant2, '\0', sizeof( nro_estacion_ant2 ));

    // Se copia la linea capturada para operar con "strtok" sobre la copia 
    // (evita problemas de punteros).
    strcpy( linea_copia, linea );

    if( contador_linea == 4 ) //< Primera linea del archivo "datos_metASCII.csv"
    						  //  donde hay datos generados por alguna estación.
	{
		// Se parsea el numero de la estacion.
		dato_linea = strtok( linea_copia, "," );

		// El nro de estacion se copia a la variable "numero de estacion". 
		strcpy( nro_estacion_ant, dato_linea );
		strcpy( nro_estacion_save, dato_linea );

		// Se ingresa el numero de estacion al archivo de numeros de estaciones.
		nros_estaciones = fopen( "nros_estaciones", "w" );
		fputs( nro_estacion_save, nros_estaciones );
		fclose( nros_estaciones );

		// Se genera un archivo "nro_estacion" donde se guardan las lineas 
		// de datos de esa estacion. 
		datos_estacion = fopen( nro_estacion_ant, "w" );
		
		dato_linea = strtok( linea, "\n" );
		fputs( dato_linea, datos_estacion );

		fclose( datos_estacion );
	}

	// Se opera sobre todas las demas lineas del archivo de datos meteorologicos.
	else if( contador_linea > 4 )
	{
		// Se captura el numero de la estacion.
		dato_linea = strtok( linea_copia, "," );
		// El nro de estacion se copia a la variable "numero de estacion". 
		strcpy( nro_estacion_sig, dato_linea );
		strcpy( nro_estacion_save, dato_linea );

		// Se obtiene el último número de estación almacenado en el 
		// archivo "nros_estaciones"
		nros_estaciones = fopen( "nros_estaciones", "r" );

		while( fgets( nro_estacion_ant2, 1000, ( FILE *)nros_estaciones) != NULL )
		{
			dato_linea = strtok( nro_estacion_ant2, "\n" ); 
			strcpy( nro_estacion_ant, dato_linea);
		}

		fclose( nros_estaciones );

		// Si el numero de estacion cambió, se genera un nuevo archivo para almacenar los datos.
		if( strcmp( nro_estacion_ant, nro_estacion_sig ))
		{
			strcpy( nro_estacion_ant, nro_estacion_sig );
			datos_estacion = fopen( nro_estacion_ant, "w" );
			dato_linea = strtok( linea, "\n" );
    		fputs( dato_linea, datos_estacion );
			fclose( datos_estacion );

			// Se agrega el siguiente numero de estacion al archivo de numeros de estaciones.
    		nros_estaciones = fopen( "nros_estaciones", "a" );
    		fputc( '\n', nros_estaciones );
    		fputs( nro_estacion_save, nros_estaciones );
    		fclose( nros_estaciones );
		}

		// Si el numero de estacion anterior es el mismo que el de la estacion siguiente:
		else
		{
			// Se abre el archivo ya existente "nro_estacion" donde se guardan las lineas 
			// de datos de esa estacion. 
    		datos_estacion = fopen( nro_estacion_ant, "a" );
    		fputc( '\n', datos_estacion ); //< Se agrega un salto de línea antes de agregar otra.
    		dato_linea = strtok( linea, "\n" ); //< Se parsean los datos sin salto de linea.
    		fputs( dato_linea, datos_estacion ); //< Se insertan los datos en el archivo.
    		fclose( datos_estacion );    			
		}
	}
}