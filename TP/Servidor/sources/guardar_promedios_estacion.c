#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/guardar_promedios_estacion.h"
#include "../headers/promediar_variable.h"

#define TAM 200

void guardar_promedios_estacion( char* nro_estacion )
{
	char nro_estacion_copia[ TAM ];
    memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
    strcpy( nro_estacion_copia, nro_estacion );

    char promedio_variable[ TAM ];
    memset( promedio_variable, '\0', sizeof( promedio_variable ));

    char promedio_variable_limpio[ TAM ];
    memset( promedio_variable_limpio, '\0', sizeof( promedio_variable_limpio ));

    char linea[ TAM ];
    memset( linea, '\0', sizeof( linea ));

    int cursor_variables = 5;
    int tope_cursor = 0;

    FILE* datos_estacion = fopen( nro_estacion, "r" );
    fgets( linea, TAM, ( FILE* ) datos_estacion );
	char* parseo = strtok( linea, "," );

	// Este bucle cuenta la cantidad de datos en una linea de datos de estacion
	// para definir cuantas iteraciones son necesarias en el calculo de los promedios.
	while( strcmp( parseo, "\n" )) //> Mientras "parseo" no haya llegado al final de linea...
	{
		parseo = strtok( NULL, "," );
		tope_cursor++;
	}

	fclose( datos_estacion );

	// Generación de archivo donde se almacenarán los promedios de las variables
	// de la estación.
	FILE* promedios_estacion = fopen( strcat( nro_estacion_copia, "_promedios" ), "w" );

	// Se guarda en el archivo de promedios
	for(cursor_variables; cursor_variables <= tope_cursor; cursor_variables++ )
	{
		promediar_variable( nro_estacion, cursor_variables, promedio_variable );
		
		// Se chequea que "promediar_variable" haya rellenado el string de promedio
		// y que no lo haya devuelto "limpio".
		if( strcmp( promedio_variable, promedio_variable_limpio )) 
		{
			if( cursor_variables > 5 )
			{
				fputs( "\n", promedios_estacion);		
			}
			fputs( promedio_variable, promedios_estacion);		
			memset( promedio_variable, '\0', sizeof( promedio_variable ));
		}
	}

	fclose( promedios_estacion );
}