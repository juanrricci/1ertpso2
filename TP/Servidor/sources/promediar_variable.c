#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/promediar_variable.h"

#define TAM 200

void promediar_variable( char* nro_estacion, int cursor_variables, char* promedio_variable )
{
	// Si "cursor variables" es igual a 10, significa que se está apuntando a la variable
	// "direccion del viento", la cual no puede ser promediada. Por lo tanto,
	// se sale de la funcion.
	if( cursor_variables == 10 )
	{
		return;
	}

	char linea[ TAM ];
    memset( linea, '\0', sizeof( linea ));

    char acumulado_char[ TAM ];
    memset( acumulado_char, '\0', sizeof( acumulado_char ));

    double acumulado_double = 0.0;
    int contador_lineas = 0;

	// Apertura del archivo para lectura de los datos de la estación.
	FILE* datos_estacion = fopen( nro_estacion, "r" );

	// Se recorre el archivo de datos de la estacion hasta el final.
	while( fgets( linea, TAM, ( FILE* )datos_estacion) != NULL )
	{
		// Se parsea el primer dato de la linea (nro de estacion)
		char* parseo = strtok( linea, "," );

     	for( int contador_datos = 2; contador_datos <= cursor_variables; contador_datos++ )
     	{
     		// Se parsean los datos de la linea hasta llegar al dato apuntado por
     		// "cursor_variables"
     		parseo = strtok( NULL, "," );
     	}

     	// Si la variable parseada es distinta de "--", se prosigue con 
     	// el calculo del promedio de esa variable. De lo contrario, 
     	// se sale de la funcion.
     	if( strcmp( parseo, "--" ))
     	{
     		acumulado_double += atof( parseo );
			contador_lineas++;
     	}

     	else
     	{
     		fclose( datos_estacion );
     		return;
     	}
	}

	// Se calcula el promedio de la variable 
	acumulado_double /= contador_lineas;
	// Se convierte el promedio en string
	sprintf( acumulado_char, "%g", acumulado_double );

	strcpy( promedio_variable, acumulado_char );

	fclose( datos_estacion );
}