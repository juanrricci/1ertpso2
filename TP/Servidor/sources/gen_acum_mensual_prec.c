#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/gen_acum_mensual_prec.h"
#include "../headers/gen_acum_diario_prec.h"

#define TAM 200

void gen_acum_mensual_prec( char* nro_estacion )
{
	gen_acum_diario_prec( nro_estacion );

	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	char linea[ TAM ];
	memset( linea ,'\0', sizeof( linea ));

	char mes_anterior[ TAM ];
	memset( mes_anterior, '\0', sizeof( mes_anterior ));

	char mes_siguiente[ TAM ];
	memset( mes_siguiente, '\0', sizeof( mes_siguiente ));

	char acumulado_char[ TAM ];
	memset( acumulado_char, '\0', sizeof( acumulado_char ));

	double acumulado_double;

	// Apertura del archivo con los valores de precipitaciones diarias acumuladas de la estacion.
	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acum_diario = fopen( strcat( nro_estacion_copia, "_acum_diario" ), "r" );

	// Apertura del archivo donde se almacena el acumulado diario.
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acumulado_mensual = fopen( strcat( nro_estacion_copia, "_acum_mensual" ), "w" );

	// Relevo del primer mes almacenado en el archivo de acumulados diarios.
	fgets( linea, TAM,( FILE* )acum_diario );
	char* parseo = strtok( linea, "/" );
	parseo = strtok( NULL, ":" );
	strcpy( mes_anterior, parseo );

	rewind( acum_diario );

	// Bucle de acumulacion mensual de precipitaciones
	while( fgets( linea, TAM,( FILE* )acum_diario ) != NULL)
	{
		parseo = strtok( linea, "/" );
		parseo = strtok( NULL, ":" );
		strcpy( mes_siguiente, parseo );

		// Si el mes relevado anteriormente es distinto del relevado en el paso 
		// actual, se almacena el acumulado mensual de precipitacion
		if( strcmp( mes_anterior, mes_siguiente ))
		{
			strcat( mes_anterior, ": " );

			// Casteo del valor double a string y guardado en acumulado_char
			sprintf( acumulado_char, "%g", acumulado_double );
			strcat( mes_anterior, acumulado_char );
			strcat( mes_anterior, "\n" );

			// Guardado del acumulado en el archivo.
			fputs( mes_anterior, acumulado_mensual );

			// Actualizacion del "dia anterior" y borrado del double acumulado.			
			strcpy( mes_anterior, mes_siguiente );
			acumulado_double=0.0;
		}

		// Se parsea el valor de precipitacion y se lo acumula en el double 
		// acumulado.
		//parseo = strtok( NULL, " " );
		parseo = strtok( NULL, "\n" );
		acumulado_double += atof( parseo );
	}

	// Se repiten los pasos para almacenar el último acumulado
	strcat( mes_anterior, ": " );
	sprintf( acumulado_char, "%g", acumulado_double );
	strcat( mes_anterior, acumulado_char );
	fputs( mes_anterior, acumulado_mensual );

	// Se cierran los archivos utilizados.
	fclose( acum_diario );
	fclose( acumulado_mensual );
}