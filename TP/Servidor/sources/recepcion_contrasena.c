#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../headers/recepcion_contrasena.h"
#include "../headers/leer_tcp.h"
#include "../headers/escribir_tcp.h"

#define TAM 256

void recepcion_contrasena( int newsockfd )
{
	char contrasena[ TAM ];
	int n = leer_tcp( newsockfd, contrasena );

	printf( "PROCESO %d. ", getpid() );
	printf( "Recibí contraseña: %s", contrasena );

	if (strcmp( contrasena, "pedro\n" ))
	{
		n = escribir_tcp( newsockfd, "Contraseña invalida" );		
	}

	else
	{
		n = escribir_tcp( newsockfd, "Contraseña valida" );		
	}
}