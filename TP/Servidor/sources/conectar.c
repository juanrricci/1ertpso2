#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "../headers/conectar.h"
#include "../headers/generar_socket_tcp.h"
#include "../headers/hijo.h"

int conectar()
{
	int sockfd, newsockfd, clilen, pid;
	struct sockaddr_in cli_addr;

	sockfd = generar_socket_tcp( &clilen, ( struct sockaddr* ) &cli_addr );

	// struct sockaddr_in serv_addr;
	// int tamano_direccion;

	// int newsockfd_udp = generar_socket_udp( &serv_addr, &tamano_direccion );

	// Generación de fork con cada conexión de cliente.
	while( 1 ) {
		newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );
		if ( newsockfd < 0 ) {
			perror( "accept" );
			exit( 1 );
		}

		pid = fork(); //< Fork para generar proceso hijo.
		if ( pid < 0 ) {
			perror( "fork" );
			exit( 1 );
		}

		if ( pid == 0 ) {  //< Proceso hijo
			hijo( sockfd, newsockfd );
		}

		else //< Proceso padre.
		{
			printf( "SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid );
			close( newsockfd );
		}
	}
	return 0; 
} 
