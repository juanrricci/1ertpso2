#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "../headers/leer_tcp.h"

#define TAM 256

int leer_tcp( int newsockfd, char* buffer )
{
	int n = read( newsockfd, buffer, TAM-1 );
	if ( n < 0 ) {
		perror( "lectura de socket" );
		exit(1);
	}

	return n;
}
